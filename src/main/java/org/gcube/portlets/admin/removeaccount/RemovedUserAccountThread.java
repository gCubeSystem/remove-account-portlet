package org.gcube.portlets.admin.removeaccount;

import org.gcube.portal.removeaccount.D4ScienceRemoveAccountManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 
 * @author Massimiliano Assante ISTI-CNR
 *
 */
public class RemovedUserAccountThread implements Runnable {

	private static final Logger _log = LoggerFactory.getLogger(RemovedUserAccountThread.class);
	private String userNameToDelete;


	

	public RemovedUserAccountThread(String userNameToDelete) {
		super();
		this.userNameToDelete = userNameToDelete;
	}

	@Override
	public void run() {
		try {
			
			D4ScienceRemoveAccountManager removeAccountManager = new D4ScienceRemoveAccountManager(userNameToDelete);
			boolean result = removeAccountManager.doAsyncRemoveAccount();
			_log.info("The user "+userNameToDelete+ " removed her/his account with success?"+ result);	
				
		} catch (Exception e) {
			_log.error("An error occurred during user workspace removal: ", e);
		}
	}
}
