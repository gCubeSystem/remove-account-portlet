package org.gcube.portlets.admin;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.ProcessAction;

import org.gcube.portal.removeaccount.D4ScienceRemoveAccountManager;
import org.gcube.portlets.admin.removeaccount.RemovedUserAccountThread;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class RemoveAccountPortlet
 */
public class RemoveAccountPortlet extends MVCPortlet {
	private static Log _log = LogFactoryUtil.getLog(RemoveAccountPortlet.class);
	public static final String AUTORISED_INFRA_ROLE = "Infrastructure-Manager";
	
	@ProcessAction(name = "deleteAccount")
	public void deleteAccount(ActionRequest actionRequest, ActionResponse response) throws IOException, PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY); 
		System.out.println("\n\n\n****Sending Response ="+themeDisplay.getURLSignOut());
		response.sendRedirect(themeDisplay.getURLSignOut());
		String username2Delete = null;
		try {
			username2Delete = PortalUtil.getUser(actionRequest).getScreenName();
		} catch (Exception e) {
			e.printStackTrace();
		}	
		System.out.println("\n The Response sent ...");
		if (username2Delete != null) {
			Thread dropUserAccountThread = new Thread(new RemovedUserAccountThread(username2Delete));
			dropUserAccountThread.start();
		} else  {
			_log.error("Account not removed");
		}			
	}
}
